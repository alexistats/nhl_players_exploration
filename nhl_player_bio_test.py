
import pandas as pd

## We have to reload the data
data = pd.read_csv("nhl_bio_2020.csv")
data.head()
data = data[['first_name', 'last_name', 'height','weight', 'position_name', 'team_name']]

## Need to perform data manipulation to set weight in kg and heigh in cm
data['weight'] = round(data['weight'] * 0.453592,1)
data['height'] = data['height'].str[0:1].astype(int)*30.48 + data['height'].str[2:5].replace('"','', regex=True).astype(int)*2.54


##################################################################################
## Let's explore the sizes and weights by positions
##################################################################################

by_position = data.groupby('position_name').mean()
print(by_position)

by_position = by_position.sort_values('height')
by_position.plot.barh(y=['height', 'weight'])

## the values are very close, but can we compare them using t tests?
from scipy import stats
data_defensemen = data[data['position_name'] == 'Defenseman']
data_goalies = data[data['position_name'] == 'Goalie']
forwards = ['Right Wing', 'Center', 'Left Wing']
data_forwards = data[data['position_name'].isin(forwards)]

## Assumptions of normality
stats.shapiro(data_defensemen['height'])
stats.shapiro(data_forwards['height'])
stats.shapiro(data_goalies['height'])

ttest, pval = ttest_ind(data_defensemen['height'], data_forwards['height'], equal_var = False)
print('p_value is ', pval)

if pval < 0.05:
    print("There is significant evidence that NHL defensemen are taller than forwards")
else:
    print("There is no statistical difference between the size of NHL defensemen and forwards")

ttest, pval = ttest_ind(data_defensemen['height'], data_goalies['height'], equal_var = False)
print('p_value is ', pval)

if pval < 0.05:
    print("There is significant evidence that NHL goalies are taller than defensemen")
else:
    print("There is no statistical difference between the size of NHL goalies and defensemen")

ttest, pval = ttest_ind(data_forwards['height'], data_goalies['height'], equal_var = False)
print('p_value is ', pval)

if pval < 0.05:
    print("There is significant evidence that NHL goalies are taller than forwards")
else:
    print("There is no statistical difference between the size of NHL goalies and forwards")

## Now weight
## Assumptions of normality
stats.shapiro(data_defensemen['weight'])
stats.shapiro(data_forwards['weight'])
stats.shapiro(data_goalies['weight'])

ttest, pval = ttest_ind(data_defensemen['weight'], data_forwards['weight'], equal_var = False)
print('p_value is ', pval)

if pval < 0.05:
    print("There is significant evidence that NHL defensemen weight more than forwards")
else:
    print("There is no statistical difference between the weight of NHL defensemen and forwards")


ttest, pval = ttest_ind(data_goalies['weight'], data_forwards['weight'], equal_var = False)
print('p_value is ', pval)

if pval < 0.05:
    print("There is significant evidence that NHL goalies weight more than forwards")
else:
    print("There is no statistical difference between the weight of NHL goaliesand forwards")

ttest, pval = ttest_ind(data_defensemen['weight'], data_goalies['weight'], equal_var = False)
print('p_value is ', pval)

if pval < 0.05:
    print("There is significant evidence that NHL defensemen weight more than goalies")
else:
    print("There is no statistical difference between the weight of NHL defensemen and goalies")


##################################################################################
## Did playoff teams have taller, bigger players?
###################################################################################
playoff_teams = ["New York Islanders", "Montréal Canadiens", "Toronto Maple Leafs", "Winnipeg Jets", "Edmonton Oilers", "Vegas Golden Knights", "Colorado Avalanche",
                "Washington Capitals", "Boston Bruins", "Florida Panthers", "Pittsburgh Penguins", "Carolina Hurricanes", "Nashville Predators", "Tampa Bay Lightning",
                "St. Louis Blues", "Minnesota Wild"]

data_playoffs = data[data['team_name'].isin(playoff_teams)]
data_not_playoff = data[~data['team_name'].isin(playoff_teams)]

print(data_playoffs.mean())
print(data_not_playoff.mean())

ttest, pval = ttest_ind(data_playoffs['height'], data_not_playoff['height'], equal_var = False)
print('p_value is ', pval)

if pval < 0.05:
    print("There is significant evidence that playoff teams are taller than non playoff teams")
else:
    print("There is no statistical difference between the size of playoff teams and non playoff teams")


## Weight
ttest, pval = ttest_ind(data_playoffs['weight'], data_not_playoff['weight'], equal_var = False)
print('p_value is ', pval)

if pval < 0.05:
    print("There is significant evidence that playoff teams are heavier than non playoff teams")
else:
    print("There is no statistical difference between the size of playoff teams and non playoff teams")