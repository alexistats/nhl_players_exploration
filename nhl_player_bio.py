#### 130 Projects: Project 1 ######

## The full list of original projects can be found here:

import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import seaborn as sns

data = pd.read_csv("nhl_bio_2020.csv")
data = data[['first_name', 'last_name', 'height','weight', 'position_name']]
print(data.head())

##################################################################################
## First Data Exploration: Manually Calculating Statistics
##################################################################################

## Save the height and weight columns as a numpy array to compute summary statistics
height = np.array(data["height"])
weight = np.array(data["weight"])
print(height[0:10])
print(weight[0:10])

## We see that heights are in feet, and weight in pounds.
## We will convert weight to kg, and height to cm - we also need to convert it from str to int
x = 0
for item in weight:
    weight[x] = round(item * 0.453592, 1)
    x+= 1

x = 0
for item in height:
    height[x] = int(item[0:1])*30.48 + int(item[2:item.find('"')])*2.54
    x += 1


## Perform different summary statistics on the data
print("Mean of heights = ", height.mean())
print("Standard Deviation of height = ", height.std())
print("Minimum height = ", height.min())
print("Maximum height = ", height.max())
print("25th percentile =", np.percentile(height, 25))
print("Median = ", np.median(height))
print("75th percentile = ", np.percentile(height, 75))

print("Mean of weights = ", weight.mean())
print("Standard Deviation of weight = ", weight.std())
print("Minimum weight = ", weight.min())
print("Maximum weight = ", weight.max())
print("25th percentile =", np.percentile(weight, 25))
print("Median = ", np.median(weight))
print("75th percentile = ", np.percentile(weight, 75))

## We can also plot the data
plt.boxplot(height)
plt.title("Heights of NHL Players")
plt.xlabel("height(cm)")
plt.show()

plt.boxplot(height)
plt.title("Weights of NHL Players")
plt.xlabel("weight(kg)")
plt.show()


## And a histogram
sns.set()
plt.hist(height)
plt.title("Height Distribution of NHL Players")
plt.xlabel("height(cm)")
plt.ylabel("Number")
plt.show()

sns.set()
plt.hist(height)
plt.title("Weight Distribution of NHL Players")
plt.xlabel("weight(kg)")
plt.ylabel("Number")
plt.show()

##################################################################################
## Second Data Exploration: Using Pandas df and Describe()
##################################################################################
pd.options.display.max_columns = None
pd.options.display.width=None

## As an alternative, when loading the csv file with pandas, simply use pandas and describe
data = pd.read_csv("nhl_bio_2020.csv")
data.head()
data = data[['first_name', 'last_name', 'height','weight', 'position_name', 'team_name']]

## Need to perform data manipulation to set weight in kg and heigh in cm
data['weight'] = round(data['weight'] * 0.453592,1)
data['height'] = data['height'].str[0:1].astype(int)*30.48 + data['height'].str[2:5].replace('"','', regex=True).astype(int)*2.54

## Describe gives everything that was calculated above - without the graphs
data["height"].describe()
data["weight"].describe()

##################################################################################
## Third Data Exploration: Using Pandas Profiling
##################################################################################
from pandas_profiling import ProfileReport
## Pandas profiling generates a nice report of the data exploration

prof = ProfileReport(data)
prof.to_file(output_file='../pandas_profiling.html')
