#### 130 Projects: Project 1 ######

## The full list of original projects can be found here:
## https://thecleverprogrammer.com/2020/05/08/data-science-project-on-president-heights/

import pandas as pd
import requests
import json

## Let's collect the data from the NHL's api

## We can collect the player's height and weight from their bio, and to access their bio, we need to access each team's roster.
## The "teams" endpoint returns active NHL teams, we will loop through each team and loop through the rosters to find the height and weight.
response = requests.get("https://statsapi.web.nhl.com/api/v1/teams").text
## We obtain a json response that we convert into a dictionary like so:
teams_dict = json.loads(response)
## remove last team - Seattle - since no players yet
teams_dict["teams"].pop()

player_list = []
for item in teams_dict["teams"]:
    team_id = item["id"] # obtain the team id
    team_name = item["name"]
    team_roster = requests.get("https://statsapi.web.nhl.com/api/v1/teams/" + str(team_id) + "/roster").text
    team_roster = json.loads(team_roster)
    for player in team_roster["roster"]:
        player_id = player["person"]["id"]
        player_page = requests.get("https://statsapi.web.nhl.com/api/v1/people/" + str(player_id)).text
        player_page = json.loads(player_page)
        ## using .get on dictionary so if there is no key, then returns nonw
        player_list.append(["2020",
                            team_id,
                            team_name,
                            player_page["people"][0].get("id"),
                            player_page["people"][0].get("firstName"),
                            player_page["people"][0].get("lastName"),
                            player_page["people"][0].get("primaryNumber"),
                            player_page["people"][0].get("birthDate"),
                            player_page["people"][0].get("currentAge"),
                            player_page["people"][0].get("birthCity"),
                            player_page["people"][0].get("birthStateProvince"),
                            player_page["people"][0].get("birthCountry"),
                            player_page["people"][0].get("nationality"),
                            player_page["people"][0].get("height"),
                            player_page["people"][0].get("weight"),
                            player_page["people"][0].get("active"),
                            player_page["people"][0].get("alternateCaptain"),
                            player_page["people"][0].get("captain"),
                            player_page["people"][0].get("rookie"),
                            player_page["people"][0].get("shootsCatches"),
                            player_page["people"][0].get("rosterStatus"),
                            player_page["people"][0]["primaryPosition"]["name"],
                            player_page["people"][0]["primaryPosition"]["type"],
                            player_page["people"][0]["primaryPosition"]["abbreviation"]])

player_df = pd.DataFrame(data=player_list, columns = ["start_year", "team_id", "team_name", "id", "first_name", "last_name", "primary_number", "birth_date", "current_age",
                                                       "birth_city", "birth_state_province", "birth_country", "nationality", "height", "weight",
                                                       "active", "alternate_captain", "captain", "rookie", "shoots_catches", "roster_status",
                                                       "position_name", "position_type", "position_abbreviation"])

player_df.head()
player_df.to_csv("nhl_bio_2020.csv")
